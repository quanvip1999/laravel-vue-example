<?php

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::truncate();

        $json = file_get_contents('./database/seeds/dbseed.json');
        $events = json_decode($json, true);

        foreach ($events['events'] as $event) {
            Event::create([
                'title' => $event['title'],
                'date' => $event['date'],
                'time' => $event['time'],
                'location' => $event['location'],
                'description' => $event['description'],
                'organizer' => $event['organizer'],
                'category' => $event['category'],
                'attendees' => $event['attendees']
            ]);
        }
    }
}

## Project setup
```
npm install
composer install
php artisan migrate
php artisan db:seed
npm run dev

```
## Run Project
```
php artisan serve
```

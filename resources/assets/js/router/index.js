import Vue from "vue"
import VueRouter from "vue-router"
import EventCreate from "../views/EventCreate.vue"
import EventList from "../views/EventList.vue"
import EventShow from "../views/EventShow.vue"
import EventEdit from "../views/EventEdit.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "event-list",
    component: EventList
  },
  {
    path: "/event/create",
    name: "event-create",
    component: EventCreate
  },
  {
    path: "/event/:id",
    name: "event-show",
    component: EventShow,
    props: true
  },
  {
    path: "/event/edit/:id",
    name: "event-edit",
    component: EventEdit,
    props: true
  }
]

const router = new VueRouter({
  mode: "hash",
  routes
})

export default router

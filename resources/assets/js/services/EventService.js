import axios from "axios"

const apiClient = axios.create({
  baseURL: "http://localhost:8000",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
})

export default {
  getEvents(page) {
    return apiClient.get("/events?page=" + page)
  },
  getEvent(id) {
    return apiClient.get("/events/" + id)
  },
  postEvent(event) {
    return apiClient.post("/events", event)
  },
  putEvent(event) {
    return apiClient.put("/events/" + event.id, event)
  },
  deleteEvent(id) {
    return apiClient.delete("/events/" + id)
  }
}
